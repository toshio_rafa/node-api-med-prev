'use-strict'

const ValidationContract = require('../validators/fluent-validator');
const repository = require('../repositories/customer-repository');
const md5 = require('md5');
const authService = require('./../services/auth-service');
const e = require('express');
const config = require('./../config');
const { sanitizeCpfCnpj } = require('./../lib/sanitization');


exports.authenticate = async (req, res, next) => {
    try {
        const customer = await repository.authenticate({
            email: req.body.email,
            password: md5(req.body.password + config.SALT_KEY)
        });

        if (!customer) {
            res.status(404).send({
                message: 'Usuário ou senha inválidos.'
            });
        }

        const token = await authService.generateToken({
            email: customer.email,
            nome: customer.nome
        });

        res.status(200).send({
            token: token,
            data: {
                email: customer.email,
                nome: customer.nome,
            }
        });
    } catch (error) {
        console.log(error)
        res.status(500).send({
            message: 'Falha ao processar requisição'
        });
    }
};


exports.post = async (req, res, next) => {
    const contract = new ValidationContract();
    contract.hasTipo(req.body.tipo);
    contract.addressValidation(req.body.enderecos);
    contract.isEmail(req.body.email);
    contract.hasPwd(req.body.password);
    req.body.tipo == 'PJ' ? contract.evenPJ(req.body) : contract.evenPF(req.body);

    if (!contract.isValid()) {
        res.status(400).send(contract.errors()).end();
        return;
    }

    try {
        await repository.create({
            tipo: req.body.tipo,
            nome: req.body.nome,
            razaoSocial: req.body.razaoSocial,
            CPF: sanitizeCpfCnpj(req.body.CPF),
            CNPJ: sanitizeCpfCnpj(req.body.CNPJ),
            nascimento: req.body.nascimento,
            email: req.body.email,
            password: md5(req.body.password + config.SALT_KEY),
            telefone: req.body.telefone,
            celular: req.body.celular,
            foto: req.body.foto,
            enderecos: req.body.enderecos
        });

        res.status(201).send({
            message: 'Customer saved!'
        });

    } catch (error) {
        console.log(error)
        res.status(500).send({
            message: 'Falha ao processar requisição'
        });
    }
};

exports.getAll = async (req, res, next) => {
    try {
        const data = await repository.getAll();
        res.status(200).send(data);
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar requisição'
        });
    }
};

exports.get = async (req, res, next) => {
    try {
        const data = await repository.get(req.params.tipo);
        res.status(200).send(data);
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar requisição'
        });
    }
};

exports.getById = async (req, res, next) => {
    try {
        const data = await repository.getById(req.params.id);
        res.status(200).send(data);
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar requisição'
        });
    }
};


exports.put = async (req, res, next) => {
    const contract = new ValidationContract();
    contract.hasTipo(req.body.tipo);
    contract.addressValidation(req.body.enderecos);
    contract.isEmail(req.body.email);
    contract.hasPwd(req.body.password);
    req.body.tipo == 'PJ' ? contract.evenPJ(req.body) : contract.evenPF(req.body);

    if (!contract.isValid()) {
        res.status(400).send(contract.errors()).end();
        return;
    }

    try {
        await repository.update(req.params.id, req.body);
        res.status(200).send({
            message: 'Customer updated!'
        });
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar requisição'
        });
    }
}


exports.delete = async (req, res, next) => {
    try {
        await repository.delete(req.params.id);
        res.status(200).send({
            message: 'Customer removed!'
        });
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar requisição'
        });
    }
};