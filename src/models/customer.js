'use-strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    tipo: {
        type: String,
        enum: ['PF', 'PJ'],
        required: [true, 'Tipo is required']
    },
    nome: {
        type: String,
        required: [true, 'Tipo is required']
    },
    razaoSocial: {
        type: String
    },
    CPF: {
        type: String
    },
    CNPJ: {
        type: String
    },
    sexo: {
        type: String
    },
    nascimento: {
        type: String
    },
    email: {
        type: String
    },
    password: {
        type: String,
        required: [true, 'Password is required']
    },
    telefone: {
        type: String
    },
    celular: {
        type: String
    },
    foto: {
        type: String
    },
    // enderecos podem ser tratados como value object sendo substituidos por inteiro a cada alteracao.
    enderecos: [
        {
            endereco: {
                type: String,
                required: [true, 'Endereco is required']
            },
            numero: {
                type: String,
                required: [true, 'Numero is required']
            },
            complemento: {
                type: String
            },
            bairro: {
                type: String
            },
            cidade: {
                type: String,
                required: [true, 'Cidade is required']
            },
            estado: {
                type: String,
                required: [true, 'Estado is required']
            },
            cep: {
                type: String,
                required: [true, 'Cep is required']
            },
        }
    ]

});

module.exports = mongoose.model('Customer', schema);