'use-stric'

const express = require('express');
const router = express.Router();
const controller = require('../controllers/customer-controller');
const authService = require('./../services/auth-service');


router.post('/authenticate', controller.authenticate);
router.post('/', controller.post);

router.get('/', authService.authorize, controller.getAll);
router.get('/tipo/:tipo', authService.authorize, controller.get);
router.get('/id/:id', authService.authorize, controller.getById);
router.put('/:id', authService.authorize, controller.put);
router.delete('/:id', authService.authorize, controller.delete);

module.exports = router;