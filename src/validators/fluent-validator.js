'use strict';

const { validateCNPJ, validateCPF } = require('./../lib/sanitization');

let errors = [];

function ValidationContract() {
    errors = [];
}

ValidationContract.prototype.evenPJ = (params) => {
    if (!params.razaoSocial || params.razaoSocial.length <= 0) {
        errors.push({ message: 'Campo razão social é obrigatorio' });
    }
    if (!params.CNPJ || params.CNPJ.length <= 0) {
        errors.push({ message: 'Campo CNPJ é obrigatorio' });
    }
    if (!validateCNPJ(params.CNPJ)) {
        errors.push({ message: 'Campo CNPJ não é valido' });
    }
}

ValidationContract.prototype.evenPF = (params) => {
    if (!params.sexo || params.sexo.length <= 0) {
        errors.push({ message: 'Campo sexo é obrigatorio' });
    }
    if (!params.CPF || params.CPF.length <= 0) {
        errors.push({ message: 'Campo CPF é obrigatorio' });
    }
    if (!validateCPF(params.CPF)) {
        errors.push({ message: 'Campo CPF não é valido' });
    }
    if (!params.nascimento || params.nascimento.length <= 0) {
        errors.push({ message: 'Campo nascimento é obrigatorio' });
    }
}

ValidationContract.prototype.hasTipo = (value) => {
    if (!value || value.length <= 0)
        errors.push({ message: 'Tipo é obrigatorio' });
}

ValidationContract.prototype.hasPwd = (value) => {
    if (!value || value.length <= 0)
        errors.push({ message: 'Password é obrigatorio' });
}

ValidationContract.prototype.addressValidation = (params) => {

    for (let index = 0; index < params.length; index++) {
        if (!params[index].endereco || params[index].endereco.length <= 0) {
            errors.push({ message: 'Campo endereco é obrigatorio: index: ' + index });
        }
        if (!params[index].numero || params[index].numero.length <= 0) {
            errors.push({ message: 'Campo numero é obrigatorio: index: ' + index });
        }
        if (!params[index].cidade || params[index].cidade.length <= 0) {
            errors.push({ message: 'Campo cidade é obrigatorio: index: ' + index });
        }
        if (!params[index].estado || params[index].estado.length <= 0) {
            errors.push({ message: 'Campo estado é obrigatorio: index: ' + index });
        }
        if (!params[index].cep || params[index].cep.length <= 0) {
            errors.push({ message: 'Campo cep é obrigatorio: index: ' + index });
        }
    }
}

ValidationContract.prototype.isRequired = (value, message) => {
    if (!value || value.length <= 0)
        errors.push({ message: message });
}

ValidationContract.prototype.hasMinLen = (value, min, message) => {
    if (!value || value.length < min)
        errors.push({ message: message });
}

ValidationContract.prototype.hasMaxLen = (value, max, message) => {
    if (!value || value.length > max)
        errors.push({ message: message });
}

ValidationContract.prototype.isFixedLen = (value, len, message) => {
    if (value.length != len)
        errors.push({ message: message });
}

ValidationContract.prototype.isEmail = (value, message) => {
    var reg = new RegExp(/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/);
    if (value && value.length > 0 && !reg.test(value)) {
        errors.push({ message: 'Formato de email inconsistente' });
    }
    if (!value || value.length <= 0) {
        errors.push({ message: 'Campo email é obrigatorio' });
    }
}

ValidationContract.prototype.errors = () => {
    return errors;
}

ValidationContract.prototype.clear = () => {
    errors = [];
}

ValidationContract.prototype.isValid = () => {
    return errors.length == 0;
}

module.exports = ValidationContract;