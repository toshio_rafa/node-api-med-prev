'use-strict'

const mongoose = require('mongoose');
const Customer = mongoose.model('Customer');
const { sanitizeCpfCnpj } = require('./../lib/sanitization');
const PF = 'tipo nome CPF sexo nascimento email telefone celular foto enderecos';
const PJ = 'tipo nome razaoSocial CNPJ email telefone celular foto enderecos';
const PFPJ = 'tipo nome CPF sexo nascimento razaoSocial CNPJ email telefone celular foto enderecos';

exports.authenticate = async (data) => {
    const res = await Customer
        .findOne({
            email: data.email,
            password: data.password
        });
    return res;
}

exports.create = async (data) => {
    const customer = new Customer(data);
    await customer.save();
}

exports.get = async (tipo) => {
    const res = await Customer
        .find({
            tipo: tipo
        }, tipo == 'PF' ? PF : PJ);
    return res;
}

exports.getAll = async () => {
    const res = await Customer.find({}, PFPJ);
    return res;
}

exports.getById = async (id) => {
    const res = await Customer
        .findById({ _id: id }, PFPJ);
    return res;
}

exports.delete = async (id) => {
    await Customer.findOneAndDelete(id);
}

exports.update = async (id, data) => {
    await Customer
        .findByIdAndUpdate(id, {
            $set: {
                tipo: data.tipo,
                nome: data.nome,
                razaoSocial: data.razaoSocial,
                CPF: sanitizeCpfCnpj(data.CPF),
                CNPJ: sanitizeCpfCnpj(data.CNPJ),
                nascimento: data.nascimento,
                email: data.email,
                telefone: data.telefone,
                celular: data.celular,
                foto: data.foto,
                enderecos: data.enderecos
            }
        });
}

